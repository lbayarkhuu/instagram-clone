import React, { useRef, useEffect, useState } from "react";
import M from "materialize-css";
import "./post.scss";

const Post = ({ avatarUrl, postedUser, imgUrls, dateTime }) => {
    const carousel = useRef(null);
    const carouselInstance = useRef(null);
    const [number, setNumber] = useState(0);
    const [liked, setLiked] = useState(false)
    useEffect(() => {
        carouselInstance.current = M.Carousel.init(carousel.current, {
            fullWidth: true,
            indicators: imgUrls.length === 1 ? false : true,
        });
    }, [imgUrls]);
    return (
        <>
            <div className="collection post-title z-depth-1">
                <div className="collection-item avatar valign-wrapper">
                    <img src={avatarUrl} className="circle" />
                    <span className="title">{postedUser}</span>
                    <a className="secondary-content">
                        <i className="material-icons">more_horiz</i>
                    </a>
                </div>
            </div>
            <div className="post-content">
                <div className="carousel carousel-slider" ref={carousel}>
                    {imgUrls.map((el) => (
                        <div className="carousel-item">
                            <img src={el} />
                        </div>
                    ))}
                </div>
                {number !== 0 && (
                    <span
                        className="back"
                        onClick={() => {
                            carouselInstance.current.prev();
                            setNumber(number - 1);
                        }}
                    >
                        <i className="material-icons">chevron_left</i>
                    </span>
                )}
                {number !== imgUrls.length - 1 && (
                    <span
                        className="next"
                        onClick={() => {
                            carouselInstance.current.next();
                            setNumber(number + 1);
                        }}
                    >
                        <i className="material-icons">chevron_right</i>
                    </span>
                )}
            </div>
            <div className="post-bottom z-depth-1">
                <div className="row">
                    <span onClick={() => setLiked(!liked)}>	<i className="material-icons">{liked ? 'favorite' : 'favorite_border'}</i></span>
                    <span>	<i className="material-icons">chat_bubble_outline</i></span>
                    <span>	<i className="material-icons">send</i></span>
                    <span>	<i className="material-icons right">bookmark_border</i></span>
                </div>
                <span>
                    Liked by <b>zula</b> and <b>others</b>
                </span>
            </div>
        </>
    );
};

export default Post;