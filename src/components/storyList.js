import React, { useEffect, useRef, useState } from 'react'
import { StoryAvatar } from './storyAvatar'
import './storyList.css'

export const StoryList = ({ setIsStory }) => {
    const stories = [
        {
            avatar: "https://instagram.fuln1-2.fna.fbcdn.net/v/t51.2885-19/s150x150/134772373_105296158081707_8817015223496788585_n.jpg?_nc_ht=instagram.fuln1-2.fna.fbcdn.net&_nc_ohc=V5H8oER2APEAX9D-wyn&tp=1&oh=af7422a6aac5afa35bb39c482508305c&oe=602DB616",
            name: "enkhmaa_1234567889",
        },
        {
            avatar: "https://instagram.fuln1-2.fna.fbcdn.net/v/t51.2885-19/s150x150/118781100_116778176657415_4249605631925662123_n.jpg?_nc_ht=instagram.fuln1-2.fna.fbcdn.net&_nc_ohc=6HNBNGWLq7cAX-QZrt8&tp=1&oh=d556917d9b39adf9b9422bbd8b8d2b2f&oe=602D83D3",
            name: "binary",
        },
        {
            avatar: "https://instagram.fuln1-2.fna.fbcdn.net/v/t51.2885-19/s150x150/31694779_2076827422540473_8726333669465128960_n.jpg?_nc_ht=instagram.fuln1-2.fna.fbcdn.net&_nc_ohc=ce5Wp5uoG_0AX9Ko7ht&tp=1&oh=f157e8af29a8955360a41f7bfbf783e6&oe=602CDACB",
            name: "bilguuniikharandaa",
        },
        {
            avatar: "https://instagram.fuln1-2.fna.fbcdn.net/v/t51.2885-19/s150x150/134772373_105296158081707_8817015223496788585_n.jpg?_nc_ht=instagram.fuln1-2.fna.fbcdn.net&_nc_ohc=V5H8oER2APEAX9D-wyn&tp=1&oh=af7422a6aac5afa35bb39c482508305c&oe=602DB616",
            name: "enkhmaa_1234567889",
        },
        {
            avatar: "https://instagram.fuln1-2.fna.fbcdn.net/v/t51.2885-19/s150x150/118781100_116778176657415_4249605631925662123_n.jpg?_nc_ht=instagram.fuln1-2.fna.fbcdn.net&_nc_ohc=6HNBNGWLq7cAX-QZrt8&tp=1&oh=d556917d9b39adf9b9422bbd8b8d2b2f&oe=602D83D3",
            name: "binary",
        },
        {
            avatar: "https://instagram.fuln1-2.fna.fbcdn.net/v/t51.2885-19/s150x150/31694779_2076827422540473_8726333669465128960_n.jpg?_nc_ht=instagram.fuln1-2.fna.fbcdn.net&_nc_ohc=ce5Wp5uoG_0AX9Ko7ht&tp=1&oh=f157e8af29a8955360a41f7bfbf783e6&oe=602CDACB",
            name: "bilguuniikharandaa",
        },
        {
            avatar: "https://instagram.fuln1-2.fna.fbcdn.net/v/t51.2885-19/s150x150/134772373_105296158081707_8817015223496788585_n.jpg?_nc_ht=instagram.fuln1-2.fna.fbcdn.net&_nc_ohc=V5H8oER2APEAX9D-wyn&tp=1&oh=af7422a6aac5afa35bb39c482508305c&oe=602DB616",
            name: "enkhmaa_1234567889",
        },
        {
            avatar: "https://instagram.fuln1-2.fna.fbcdn.net/v/t51.2885-19/s150x150/118781100_116778176657415_4249605631925662123_n.jpg?_nc_ht=instagram.fuln1-2.fna.fbcdn.net&_nc_ohc=6HNBNGWLq7cAX-QZrt8&tp=1&oh=d556917d9b39adf9b9422bbd8b8d2b2f&oe=602D83D3",
            name: "binary",
        },
        {
            avatar: "https://instagram.fuln1-2.fna.fbcdn.net/v/t51.2885-19/s150x150/31694779_2076827422540473_8726333669465128960_n.jpg?_nc_ht=instagram.fuln1-2.fna.fbcdn.net&_nc_ohc=ce5Wp5uoG_0AX9Ko7ht&tp=1&oh=f157e8af29a8955360a41f7bfbf783e6&oe=602CDACB",
            name: "bilguuniikharandaa",
        },
        {
            avatar: "https://instagram.fuln1-2.fna.fbcdn.net/v/t51.2885-19/s150x150/134772373_105296158081707_8817015223496788585_n.jpg?_nc_ht=instagram.fuln1-2.fna.fbcdn.net&_nc_ohc=V5H8oER2APEAX9D-wyn&tp=1&oh=af7422a6aac5afa35bb39c482508305c&oe=602DB616",
            name: "enkhmaa_1234567889",
        },
        {
            avatar: "https://instagram.fuln1-2.fna.fbcdn.net/v/t51.2885-19/s150x150/118781100_116778176657415_4249605631925662123_n.jpg?_nc_ht=instagram.fuln1-2.fna.fbcdn.net&_nc_ohc=6HNBNGWLq7cAX-QZrt8&tp=1&oh=d556917d9b39adf9b9422bbd8b8d2b2f&oe=602D83D3",
            name: "binary",
        },
        {
            avatar: "https://instagram.fuln1-2.fna.fbcdn.net/v/t51.2885-19/s150x150/31694779_2076827422540473_8726333669465128960_n.jpg?_nc_ht=instagram.fuln1-2.fna.fbcdn.net&_nc_ohc=ce5Wp5uoG_0AX9Ko7ht&tp=1&oh=f157e8af29a8955360a41f7bfbf783e6&oe=602CDACB",
            name: "bilguuniikharandaa",
        },
        {
            avatar: "https://instagram.fuln1-2.fna.fbcdn.net/v/t51.2885-19/s150x150/134772373_105296158081707_8817015223496788585_n.jpg?_nc_ht=instagram.fuln1-2.fna.fbcdn.net&_nc_ohc=V5H8oER2APEAX9D-wyn&tp=1&oh=af7422a6aac5afa35bb39c482508305c&oe=602DB616",
            name: "enkhmaa_1234567889",
        },
        {
            avatar: "https://instagram.fuln1-2.fna.fbcdn.net/v/t51.2885-19/s150x150/118781100_116778176657415_4249605631925662123_n.jpg?_nc_ht=instagram.fuln1-2.fna.fbcdn.net&_nc_ohc=6HNBNGWLq7cAX-QZrt8&tp=1&oh=d556917d9b39adf9b9422bbd8b8d2b2f&oe=602D83D3",
            name: "binary",
        },
        {
            avatar: "https://instagram.fuln1-2.fna.fbcdn.net/v/t51.2885-19/s150x150/31694779_2076827422540473_8726333669465128960_n.jpg?_nc_ht=instagram.fuln1-2.fna.fbcdn.net&_nc_ohc=ce5Wp5uoG_0AX9Ko7ht&tp=1&oh=f157e8af29a8955360a41f7bfbf783e6&oe=602CDACB",
            name: "bilguuniikharandaa",
        },
        {
            avatar: "https://instagram.fuln1-2.fna.fbcdn.net/v/t51.2885-19/s150x150/134772373_105296158081707_8817015223496788585_n.jpg?_nc_ht=instagram.fuln1-2.fna.fbcdn.net&_nc_ohc=V5H8oER2APEAX9D-wyn&tp=1&oh=af7422a6aac5afa35bb39c482508305c&oe=602DB616",
            name: "enkhmaa_1234567889",
        },
        {
            avatar: "https://instagram.fuln1-2.fna.fbcdn.net/v/t51.2885-19/s150x150/118781100_116778176657415_4249605631925662123_n.jpg?_nc_ht=instagram.fuln1-2.fna.fbcdn.net&_nc_ohc=6HNBNGWLq7cAX-QZrt8&tp=1&oh=d556917d9b39adf9b9422bbd8b8d2b2f&oe=602D83D3",
            name: "binary",
        },
        {
            avatar: "https://instagram.fuln1-2.fna.fbcdn.net/v/t51.2885-19/s150x150/31694779_2076827422540473_8726333669465128960_n.jpg?_nc_ht=instagram.fuln1-2.fna.fbcdn.net&_nc_ohc=ce5Wp5uoG_0AX9Ko7ht&tp=1&oh=f157e8af29a8955360a41f7bfbf783e6&oe=602CDACB",
            name: "bilguuniikharandaa",
        },
    ]

    const [left, setLeft] = useState(false)
    const [right, setRight] = useState(true)
    const listRef = useRef()

    const updateScroll = (e) => {
        const scrollLeft = e.target.scrollLeft
        const clientWidth = e.target.clientWidth
        const scrollWidth = e.target.scrollWidth

        if (scrollLeft < 10) {
            setLeft(false)
        } else {
            setLeft(true)
        }

        if (scrollLeft + clientWidth > scrollWidth - 5) {
            setRight(false)
        } else {
            setRight(true)
        }
    }

    return (
        <div className="z-depth-1 story-list-container">
            <div ref={listRef} className="story-list" onScrollCapture={(e) => updateScroll(e)}>
                {stories.map(item => (<StoryAvatar {...item} setIsStory={setIsStory} />))}
            </div>
            {
                left && (
                    <span
                        className="back"
                        onClick={() => {
                            listRef.current.scrollBy({
                                left: -200,
                                behavior: 'smooth'
                            })
                        }}
                    >
                        <i className="material-icons">chevron_left</i>
                    </span>
                )
            }
            {
                right && (
                    <span
                        className="next"
                        onClick={() => {
                            listRef.current.scrollBy({
                                left: 200,
                                behavior: 'smooth'
                            })
                        }}
                    >
                        <i className="material-icons">chevron_right</i>
                    </span>
                )
            }
        </div>
    )
}
