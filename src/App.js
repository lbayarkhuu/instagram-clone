import React, { useState } from 'react'
import 'materialize-css/dist/css/materialize.min.css'

import { Header } from './components/header'
import Post from './components/post'
import { StoryList } from './components/storyList'
import { Story } from './components/story'

const App = () => {
  const [isStory, setIsStory] = useState(false);

  return (
    <div>
      {
        isStory
          ? <Story />
          : <div>
            <Header />
            <div className="container">
              <StoryList setIsStory={setIsStory} />
              <Post
                avatarUrl={"https://instagram.fuln1-2.fna.fbcdn.net/v/t51.2885-19/s150x150/103649318_649904242534100_5775310368121173844_n.jpg?_nc_ht=instagram.fuln1-2.fna.fbcdn.net&_nc_ohc=t4X3ETaL9u0AX86g-dh&tp=1&oh=b04e6747c2f4305b4afb3ac9bf92b792&oe=602EB9F1"}
                postedUser={"theqoodquote"}
                imgUrls={["https://www.bigcommerce.com/blog/wp-content/uploads/2018/06/inspirational-business-quotes-mahatma-gandhi-change-the-world-750x750.jpg?x98314"]}
              />
              <Post
                avatarUrl={"https://instagram.fuln1-2.fna.fbcdn.net/v/t51.2885-19/s150x150/103649318_649904242534100_5775310368121173844_n.jpg?_nc_ht=instagram.fuln1-2.fna.fbcdn.net&_nc_ohc=t4X3ETaL9u0AX86g-dh&tp=1&oh=b04e6747c2f4305b4afb3ac9bf92b792&oe=602EB9F1"}
                postedUser={"theqoodquote"}
                imgUrls={["https://www.bigcommerce.com/blog/wp-content/uploads/2018/06/inspirational-business-quotes-mahatma-gandhi-change-the-world-750x750.jpg?x98314"]}
              />
              <Post
                avatarUrl={"https://instagram.fuln1-2.fna.fbcdn.net/v/t51.2885-19/s150x150/103649318_649904242534100_5775310368121173844_n.jpg?_nc_ht=instagram.fuln1-2.fna.fbcdn.net&_nc_ohc=t4X3ETaL9u0AX86g-dh&tp=1&oh=b04e6747c2f4305b4afb3ac9bf92b792&oe=602EB9F1"}
                postedUser={"theqoodquote"}
                imgUrls={["https://www.bigcommerce.com/blog/wp-content/uploads/2018/06/inspirational-business-quotes-mahatma-gandhi-change-the-world-750x750.jpg?x98314", "https://www.bigcommerce.com/blog/wp-content/uploads/2018/06/inspirational-business-quotes-mahatma-gandhi-change-the-world-750x750.jpg?x98314"]}
              />
            </div>
          </div>
      }

    </div>
  )
}

export default App;